# Kubectl-shell

## A multi-architecture container image for running kubectl and shellscripts.

*Image inspired on the work of the Portainer team*



### The what

This image contains the latest build of Kubectl, so, if you use a cutting-edge stable version of it, you're covered.



### Why having a shell?

Firstly, you can debug behaviors easily.

Secondly, you can take advantage of the shell to run scripts in your cluster with tools like Tekton, for example.



### Multi-architecture

Built for AMD64 and ARM64 at the moment, the most popular architectures in servers.



### Licensing

MIT



### The developer

Developed by @andersonpem
