FROM --platform=$TARGETPLATFORM alpine:latest
ARG TARGETOS TARGETARCH

RUN apk add -U --no-cache bash bash-completion gawk openssl gpg curl jq


# Kubectl CLI
RUN echo "Fetching Kubectl for ${TARGETARCH}"
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/${TARGETARCH}/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    echo -e 'source /usr/share/bash-completion/bash_completion\nsource <(kubectl completion bash)' >>~/.bashrc

# Helm
RUN echo "Fetching Helm for ${TARGETARCH}"
RUN curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

# Linux user permissions & aliases
RUN echo 'shell:x:1000:1000:shell,,,:/home/shell:/bin/bash' > /etc/passwd && \
    echo 'shell:x:1000:' > /etc/group && \
    mkdir /home/shell && \
    echo '. /etc/profile.d/bash_completion.sh' >> /home/shell/.bashrc && \
    echo 'alias kubectl="kubectl -n default"' >> /home/shell/.bashrc && \
    echo 'alias k="kubectl"' >> /home/shell/.bashrc && \
    echo 'alias ks="kubectl -n kube-system"' >> /home/shell/.bashrc && \
    echo 'source <(kubectl completion bash)' >> /home/shell/.bashrc && \
    echo 'PS1="> "' >> /home/shell/.bashrc && \
    chown -R shell /home/shell

# CMD file
COPY src/welcome /usr/local/bin/welcome
RUN chmod +x /usr/local/bin/welcome

USER 1000
WORKDIR /home/shell

CMD ["welcome"]